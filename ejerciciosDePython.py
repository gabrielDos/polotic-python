'''
Inicio de Ejercicios PoloTic-Python

'''
# Clase 2. Ejercicio 1):
#Escriba un programa en Python que acepte el radio de un círculo del usuario y calcule el área.

#Importaciones
#
from math import pi
#Formula + ingreso de datos
#
radio = float(input("Ingrese el radio del círculo:\n"))
area = pi * radio ** 2
print(f"El área del círculo es: {area}")  

#Ejercicio 2):
#Escribe un programa Python que acepte un número entero (n) y calcule el valor de n + n + nnn

#Ingreso de datos
#
n = int(input('Ingrese un numero entero: '))

print(f'El resultado de hacer n + n*n + n*n*n es: {n + n*n + n*n*n}')

#Ejercicio 3):
#Escribe un programa en Python que acepte una cadena de carácteres y cuente el tamaño
#de la cadena y cuantas veces aparece la letra A (mayúscula y minúscula)

#Ingresar cadena
#
frase = str(input("Ingrese Cadena: "))
#Formula
#
mayusculas = frase.count("A")
minusculas = frase.count("a")

print(f"En su frase hay {mayusculas} 'A' mayusculas y {minusculas} 'a' minusculas.")

#Ejercicio 4):
#Escribe un programa Python que muestre la hora actual con una suma de dos horas adicionales

#Importar fecha
#
from datetime import datetime as dt

now = dt.now()

print(f'Hora actual: {now.hour}:{now.minute}:{now.second}')

print(f'En dos horas: {now.hour + 2}:{now.minute}:{now.second}')